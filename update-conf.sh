#!/bin/bash

#echo "This script can cause duplicate entries unless you remove the other .conf files before running this script."
#echo "Use rm ./*.conf in this directory to remove all conf files before starting."
#
#echo "This script will download an update of itself."
#read -p "Press enter to proceed, ctrl+c to exit."
#
#wget -O update-conf.sh https://gitlab.com/Daeden-JL/minecraft/-/raw/master/update-conf.sh
#
#echo "This will download the current configs in this repo to your current directory."
#read -p "Press enter to proceed, ctrl+c to cancel."
#
#wget -O bukkit.jar.conf https://gitlab.com/Daeden-JL/minecraft/-/raw/master/bukkit.jar.conf
#wget -O bungeecord.jar.conf https://gitlab.com/Daeden-JL/minecraft/-/raw/master/bungeecord.jar.conf
#wget -O cannonspigot.jar.conf https://gitlab.com/Daeden-JL/minecraft/-/raw/master/cannonspigot.jar.conf
#wget -O geyser.jar.conf https://gitlab.com/Daeden-JL/minecraft/-/raw/master/geyser.jar.conf
##wget -O magma.jar.conf https://gitlab.com/Daeden-JL/minecraft/-/raw/master/magma.jar.conf
#wget -O mohist.jar.conf https://gitlab.com/Daeden-JL/minecraft/-/raw/master/mohist.jar.conf 
#wget -O nukkitx.jar.conf https://gitlab.com/Daeden-JL/minecraft/-/raw/master/nukkitx.jar.conf 
#wget -O paper.jar.conf https://gitlab.com/Daeden-JL/minecraft/-/raw/master/paper.jar.conf 
#wget -O paper12.2.jar.conf https://gitlab.com/Daeden-JL/minecraft/-/raw/master/paper12.2.jar.conf 
#wget -O paper14.4.jar.conf https://gitlab.com/Daeden-JL/minecraft/-/raw/master/paper14.4.jar.conf 
#wget -O paper15.2.jar.conf https://gitlab.com/Daeden-JL/minecraft/-/raw/master/paper15.2.jar.conf 
#wget -O paper16.3.jar.conf https://gitlab.com/Daeden-JL/minecraft/-/raw/master/paper16.3.jar.conf 
#wget -O paper16.4.jar.conf https://gitlab.com/Daeden-JL/minecraft/-/raw/master/paper16.4.jar.conf
#wget -O paper16.5.jar.conf https://gitlab.com/Daeden-JL/minecraft/-/raw/master/paper16.5.jar.conf 
#wget -O paper17.jar.conf https://gitlab.com/Daeden-JL/minecraft/-/raw/master/paper17.jar.conf 
#wget -O paper188.jar.conf https://gitlab.com/Daeden-JL/minecraft/-/raw/master/paper188.jar.conf 
#wget -O pocketmine.jar.conf https://gitlab.com/Daeden-JL/minecraft/-/raw/master/pocketmine.jar.conf 
#wget -O snapshot.jar.conf https://gitlab.com/Daeden-JL/minecraft/-/raw/master/snapshot.jar.conf 
#wget -O spigot.jar.conf https://gitlab.com/Daeden-JL/minecraft/-/raw/master/spigot.jar.conf
#wget -O spigot16.4.jar.conf https://gitlab.com/Daeden-JL/minecraft/-/raw/master/spigot16.4.jar.conf
#wget -O spigot188.jar.conf https://gitlab.com/Daeden-JL/minecraft/-/raw/master/spigot188.jar.conf 
#wget -O spongeforge12.2.jar.conf https://gitlab.com/Daeden-JL/minecraft/-/raw/master/spongeforge12.2.jar.conf 
#wget -O travertine.jar.conf https://gitlab.com/Daeden-JL/minecraft/-/raw/master/travertine.jar.conf 
#wget -O updater.jar.conf https://gitlab.com/Daeden-JL/minecraft/-/raw/master/updater.jar.conf 
#wget -O vanilla.jar.conf https://gitlab.com/Daeden-JL/minecraft/-/raw/master/vanilla.jar.conf 
#wget -O velocity.jar.conf https://gitlab.com/Daeden-JL/minecraft/-/raw/master/velocity.jar.conf 
#wget -O waterfall.jar.conf https://gitlab.com/Daeden-JL/minecraft/-/raw/master/waterfall.jar.conf  
#wget -O purpur.jar.conf https://gitlab.com/Daeden-JL/minecraft/-/raw/master/purpur.jar.conf  
#wget -O tuinity.jar.conf https://gitlab.com/Daeden-JL/minecraft/-/raw/master/tuinity.jar.conf
#wget -O spigot-latest.jar.conf https://gitlab.com/Daeden-JL/minecraft/-/raw/master/spigot-latest.jar.conf
#wget -O spigot-unnamed.jar.conf https://gitlab.com/Daeden-JL/minecraft/-/raw/master/spigot-unnamed.jar.conf
#wget -O forge16.5.jar.conf https://gitlab.com/Daeden-JL/minecraft/-/raw/master/forge16.5.jar.conf
#
echo "Updating permissions"
chmod -R 777 ./*.conf
echo "Setting owner:group to minecraft"
chown -R minecraft:minecraft ./*.conf

echo "This script will now download all jar files from these configs."
read -p "Press enter to proceed, ctrl+c to exit."

for i in $(/bin/ls ./*.conf)
do
  echo "Downloading from $i"
  WSOURCE=$(grep 'source ' $i | cut -d " " -f3)
  WNAME=$(echo $i | cut -c3- | rev | cut -c6- | rev)
  wget -O $WNAME $WSOURCE
done

echo "Updating permissions"
chmod -R 777 ./*.jar
echo "Setting owner:group to minecraft"
chown -R minecraft:minecraft ./*.jar


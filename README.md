# minecraft

Config files for Multicraft

Most of the jar files are being provided by https://serverjars.com

---
## Install

cd (multicraft folder)

mv ./jar ./jar.old

git clone https://gitlab.com/Daeden-JL/jar.git

---

## Updates

cd (jar folder)

git reset --hard HEAD

git clean -f -d

git pull

chown -R minecraft:minecraft ./ && chmod -R 777 ./
